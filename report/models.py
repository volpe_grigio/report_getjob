from django.core.validators import FileExtensionValidator
from django.db import models
from accounts.models import Departments,CustomUser

# Create your models here.

#--------------------------------------------------------------1
#タグ
class Tag(models.Model):

    tag_name = models.CharField(verbose_name="タグ名",max_length=50,unique=True)

    class Meta:
        verbose_name_plural = "Tag"

    def __str__(self):
        return self.tag_name

#--------------------------------------------------------------2
#企業
class Company(models.Model):

    company_name = models.CharField(verbose_name="企業名",max_length=300)

    kana_name = models.CharField(verbose_name="カナ企業名",max_length=1000)

    locate = models.CharField(verbose_name="会社所在地",max_length=200)

    address_number = models.CharField(verbose_name="郵便番号",max_length=10)
    # プルダウンで選ぶようにする
    tag1 = models.ForeignKey(Tag,verbose_name="タグ1",related_name="tag1",on_delete=models.PROTECT,null=True,blank=True)

    tag2 = models.ForeignKey(Tag,verbose_name="タグ2",related_name="tag2",on_delete=models.PROTECT,null=True,blank=True)

    tag3 = models.ForeignKey(Tag,verbose_name="タグ3",related_name="tag3",on_delete=models.PROTECT,null=True,blank=True)

    exam_contents1 = models.BooleanField(verbose_name="書類選考",default=False)

    exam_contents2 = models.BooleanField(verbose_name="面接",default=False)

    exam_contents3 = models.BooleanField(verbose_name="筆記",default=False)

    exam_contents4 = models.BooleanField(verbose_name="適性", default=False)

    exam_contents5 = models.BooleanField(verbose_name="作文", default=False)

    exam_contents6 = models.CharField(verbose_name="その他",max_length=40,null=True,blank=True)

    class Meta:
        verbose_name_plural = "Company"

    def __str__(self):
        return self.company_name

#-------------------------------------------------------------3
#職種
class Occupation(models.Model):

    occupation_name = models.CharField(verbose_name="職種名",max_length=40,unique=True)

    class Meta:
        verbose_name_plural = "Occupation"

    def __str__(self):
        return self.occupation_name

#----------------------------------------------------------4
#資格
class Qualification(models.Model):

    qualif_name = models.CharField(verbose_name="資格名",max_length=60,unique=True)

    class Meta:
        verbose_name_plural = 'Qualification'

    def __str__(self):
        return self.qualif_name

#-------------------------------------------------------------5
#資格と学科
class Qualif_Depart(models.Model):

    qualif = models.ForeignKey(Qualification,
                                  verbose_name="資格ID",on_delete=models.PROTECT)

    depart = models.ForeignKey(Departments,
                                  verbose_name="関連学科ID",on_delete=models.PROTECT)

    class Meta:
        verbose_name_plural = 'Qualif_Depart'

    #def __str__(self):
     #   return self.qualif_id,self.depart_id

#-----------------------------------------------------------6
##資格と企業
class Qualif_Compa(models.Model):

    qualif = models.ForeignKey(Qualification,
                                  verbose_name="資格ID",on_delete=models.PROTECT)

    company = models.ForeignKey(Company,
                                  verbose_name="企業ID", on_delete=models.PROTECT)

    class Meta:
        verbose_name_plural = 'Qualif_Compa'

#---------------------------------------------------------------7
#求人
class Offers(models.Model):

    company = models.ForeignKey(Company,verbose_name="企業ID",on_delete=models.PROTECT)

    occupation = models.ForeignKey(Occupation,
                                      verbose_name="募集職種ID",on_delete=models.PROTECT)

    offer_pdf = models.FileField(upload_to='uploads',verbose_name="求人票",validators=[FileExtensionValidator(['pdf',])],blank=True,null=True)

    year = models.DateField(verbose_name="西暦年度",auto_now_add=True,null=True)

    class Meta:
        verbose_name_plural = "Offers"
        #unique_together = ("company_id","occupation_id")

#---------------------------------------------------------------8
#試験報告
class Exam_Report(models.Model):

    PASSED_CHOICES = (
        (True,"合格"),
        (False,"不合格"),
    )
    report_passed = models.BooleanField(choices=PASSED_CHOICES,verbose_name="合否",
                                        null=True,blank=True,help_text="合格はTrue")

    report_company = models.ForeignKey(Company,verbose_name="受験企業",on_delete=models.PROTECT)

    report_user = models.ForeignKey(CustomUser,verbose_name="受験者",on_delete=models.PROTECT)

    report_date = models.DateField(verbose_name="登録日",auto_now_add=True)

    report_pdf = models.ImageField(verbose_name="受験報告書",blank=True,null=True)
