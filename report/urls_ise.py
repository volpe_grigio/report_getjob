from django.urls import path,include
from .views import views_simizu,views_saito,views_nihei,views_ise,views_araya

app_name = 'report'

urlpatterns = [
    path('search/',views_ise.Company_searchView.as_view(),name='search'),
    path('result/',views_ise.search_resultView.as_view(),name='result'),
    path('offer/',views_ise.job_offerView.as_view(),name='offer'),
    path('report/',views_ise.output_reportView.as_view(),name='report'),
    path('list/',views_ise.all_reportView.as_view(),name='list')
    #path('',views.IndexView.as_view(),name='index'),
]