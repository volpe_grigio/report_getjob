from django import forms
from .models import *

class CreateReportForm(forms.ModelForm):
    class Meta:
        model = Exam_Report
        fields = ('report_passed','report_pdf',)

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

            for field in self.fields.values():
                field.widget.attrs['class'] = 'form-control'





# class EditReportForm(forms.ModelForm):
#     class Meta:
#         model = Exam_Report
#         fields = ('report_pdf',)

