from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from ..models import *
from ..forms_araya import *
from django.contrib import messages
# Create your views here.


class ReportDetailView(generic.TemplateView):
    model = Company, Exam_Report
    template_name = "templates_araya/details_report.html"

    def get_context_data(self, **kwargs):
        company_context = super().get_context_data(**kwargs)
        company_list = self.kwargs['pk']
        company_detail = Company.objects.get(id=company_list)

        company_context.update({
            "company_detail": company_detail,

        })
        return company_context


class ReportEditView(generic.UpdateView):
    model = Exam_Report
    template_name = "templates_araya/edit_report.html"
    # form_class = EditReportForm
    #
    # def get_success_url(self):
    #     return reverse_lazy('ReportOfExam:details_report', kwargs={'pk': self.kwargs['pk']})


class ReportListView(generic.ListView):
    model = Exam_Report
    template_name = "templates_araya/my_report.html"
    paginate = 3

    def get_context_data(self, **kwargs):
        exam_report_context = super().get_context_data(**kwargs)
        reports = Exam_Report.objects.all()

        a = reports

        exam_report_context.update({
            "report_list": a,
        })
        return exam_report_context

    #def get_queryset(self):
        #exam_reports = Exam_Report.object.filter(report_user=self.kwargs['pk']).order_by('-report_date')
        #return exam_reports


class ReportRegisterView(generic.CreateView):
    model = Exam_Report,Company
    form_class = CreateReportForm
    template_name = "templates_araya/register_report.html"
    #success_url = reverse_lazy('ReportOfExam:my_report')

    def form_valid(self, form):
        report = form.save(commit=False)
        report.report_user = self.request.user
        report.save()
        return super().form_valid(form)

    def form_invalid(self, form):
        messages.error(self.request, "失敗しました。")
        return super().form_invalid(form)





