from django.shortcuts import render
from django.views import generic
# Create your views here.
"""
class IndexView(generic.TemplateView):
    template_name = "index.html"
"""
class Company_searchView(generic.TemplateView):
    template_name = "templates_ise/company_search.html"


class search_resultView(generic.TemplateView):
    template_name = "templates_ise/search_result.html"


class job_offerView(generic.TemplateView):
    template_name = "templates_ise/job_offer.html"


class output_reportView(generic.TemplateView):
    template_name = "templates_ise/report.html"


class all_reportView(generic.TemplateView):
    template_name = "templates_ise/report_list.html"



