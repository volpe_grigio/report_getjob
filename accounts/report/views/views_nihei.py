from django.http import *
from django.shortcuts import render,resolve_url
from django.urls import reverse_lazy
from django.views import generic
from ..models import *
from django.db.models import Q
from ..forms_nihei import *
# Create your views here.

class CompanyListView(generic.ListView):
    model = Company
    template_name = "templates_nihei/company_list.html"
    
    def get_context_data(self, **kwargs):
        company_context = super().get_context_data(**kwargs)
        companies = Company.objects.all()

        a = companies

        company_context.update({
            "company_list": a,
        })
        return company_context

    def get_queryset(self):
        q_word = self.request.GET.get('query')
        if q_word:
            object_list = Company.objects.filter(
                Q(company_name__icontains=q_word))
            print(object_list)
        else:
            object_list = Company.objects.all()

        return object_list

    def post(self, request, *args, **kwargs):
        if "tag_register" in request.POST:
            response = HttpResponseRedirect("/tag_register/")

            response.set_cookie("return", "list")

            return response

class CompanyRegisterView(generic.CreateView):
    model = Qualif_Compa
    template_name = "templates_nihei/company_register.html"
    form_class = CompanyRegisterForm
    success_url = reverse_lazy('ReportOfExam:company_list')

    def form_valid(self,form):
        #messages.success(self.request,'求人票を登録しました。')
        return super().form_valid(form)

    def form_invalid(self, form):
        return super().form_invalid(form)

    def post(self, request, *args, **kwargs):
        print(request.POST)
        if "company" in request.POST:

            response = HttpResponseRedirect("/tag_register/")

            response.set_cookie("return", "tag")

            return response

        abac = Company(company_name=request.POST["company_name"],kana_name=request.POST["kana_name"],locate=request.POST["locate"],address_number=request.POST["address_number"],tag1_id=request.POST["tag1"],tag2_id=request.POST["tag2"],tag3_id=request.POST["tag3"])

        abac.save()
        return HttpResponseRedirect("/company_list/")

class TagRegisterView(generic.CreateView):
    template_name = "templates_nihei/tag_register.html"
    model = Tag
    form_class = TagRegisterForm
    success_url = reverse_lazy('ReportOfExam:tag_register')


    def form_valid(self,form):
        #messages.success(self.request,'求人票を登録しました。')
        return super().form_valid(form)

    def form_invalid(self, form):
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        tag_context = super().get_context_data(**kwargs)
        tag_list = Tag.objects.all()

        tag_context.update({
            "tags":tag_list,
        })
        return tag_context

    def post(self, request, *args, **kwargs):
        if "tag_del" in request.POST:
            response = HttpResponseRedirect("/tag_delete/")

            response.set_cookie("to_tagregi", "to_tag_register")

            return response

        if request.method == "POST":
            if "modoru" in request.POST:
                print(request.POST)

                if self.request.COOKIES.get("return") == "list":
                     return HttpResponseRedirect("/company_list/")

                else:
                    return HttpResponseRedirect("/company_register/")
        if "tag_name" in request.POST:
            aaa = request.POST["tag_name"]
            ab = Tag(tag_name=aaa)
            ab.save()
            return HttpResponseRedirect("/tag_register/")


class CompanyDetailView(generic.TemplateView):
    template_name = "templates_nihei/company_detail.html"
    model = Company,Offers,Tag

    def get_context_data(self, **kwargs):
        company_context = super().get_context_data(**kwargs)
        company_list = self.kwargs['pk']
        company_detail = Company.objects.get(id=company_list)
        if company_detail.tag1_id is not None:
            tag_name1 = Tag.objects.get(id=company_detail.tag1_id)
            company_context.update({
                "tag_name1":tag_name1,
            })
        if company_detail.tag2_id is not None:
            tag_name2 = Tag.objects.get(id=company_detail.tag2_id)
            company_context.update({
                "tag_name2":tag_name2,
            })
        if company_detail.tag3_id is not None:
            tag_name3 = Tag.objects.get(id=company_detail.tag3_id)
            company_context.update({
                "tag_name3":tag_name3,
            })
        company_context.update({
            "company_detail": company_detail,
        })
        return company_context


class OfferRegisterView(generic.CreateView):
    template_name = "templates_nihei/offer_register.html"
    model = Offers,Occupation,Company
    form_class = OfferRegisterForm
    success_url = reverse_lazy('ReportOfExam:company_detail')

    def get_success_url(self):
        return reverse_lazy('ReportOfExam:company_detail', kwargs={'pk': self.kwargs['pk']})

    def form_valid(self,form):
        offers = form.save(commit=False)
        #offers.company = self.kwargs['pk']
        offers.save()
        #messages.success(self.request,'求人票を登録しました。')
        return super().form_valid(form)

    def form_invalid(self, form):
        return super().form_invalid(form)

    def get_context_data(self, **kwargs):
        offer_register_context = super().get_context_data(**kwargs)
        occupation_list = Occupation.objects.all()

        offer_register_context.update({
            "occupation":occupation_list,
        })
        return offer_register_context

    def get(self, request,  **kwargs):
        initial_dict = dict(company=self.kwargs['pk'])
        form = OfferRegisterForm(request.GET or None, initial=initial_dict)

        return render(request, 'templates_nihei/offer_register.html', dict(form=form))



class OfferDeleteView(generic.TemplateView):
    template_name = "templates_nihei/offer_delete.html"


class TagDeleteView(generic.ListView):
    template_name = "templates_nihei/tag_delete.html"
    model = Tag

    def get_context_data(self, **kwargs):
        tag_delete_context = super().get_context_data(**kwargs)

        tag_list = Tag.objects.all()

        tag_delete_context.update({
            "tags":tag_list,
        })
        return tag_delete_context

    def get_queryset(self):
        q_word = self.request.GET.get('query')
        if q_word:
            object_list = Tag.objects.filter(
                Q(tag_name__icontains=q_word))
        else:
            object_list = Tag.objects.all()
            print(object_list)

        return object_list

    def post(self, request):
        post_pks = request.POST.getlist('delete')  # <input type="checkbox" name="delete"のnameに対応


        if request.method == "POST":
            if "modoru" in request.POST:
                if self.request.COOKIES.get("to_tagregi") == "to_tag_register":
                    return HttpResponseRedirect("/tag_register/")


            else:
                Tag.objects.filter(pk__in=post_pks).delete()
                return  HttpResponseRedirect("/tag_delete/")
