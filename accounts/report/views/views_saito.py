from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from report.forms_saito import SelectDepartForm
from accounts.models import Departments,CustomUser
from django.contrib import messages
from django.http import HttpResponseRedirect



# Create your views here.

class IndexView(generic.TemplateView):
    model = Departments
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        departs = Departments.objects.all()

        context.update({
            "departs": departs,
        })
        return context

class DepartView(generic.TemplateView):
    model = Departments,CustomUser
    template_name = "account/select_depart.html"
    form_class = SelectDepartForm
    #success_url = reverse_lazy('report:index')

    def get_context_data(self, **kwargs):
        context = generic.TemplateView().get_context_data(**kwargs)
        departs = Departments.objects.all()

        context.update({
            "departs": departs,
        })
        return context


    def form_valid(self,form):
        depart = form.save(commit=False)
        depart.user = self.request.user
        depart.save()
        messages.success(self.request,'あなたの所属学科を登録しました')
        return super().form_valid(form)

    def post(self,request,*args,**kwargs):
        fm = SelectDepartForm()
        Create = fm.save(commit=False)
        Create.id = request.user.id
        print(request.user)
        print("id={}".format(request.user.id))
        departs = Departments.objects.all()

        if request.method == "POST":
            print("---------------")
            print(request.POST)
            print(departs)
            print(departs[len(departs)-1].id)

            #id_ls = [ i for i in range(len(departs)-1) in departs ]
            #print(id_ls)

            #for vl in range(1,8):
             #   stl = str(vl)
              #  if stl == request.POST['depart']:
               #     print("{}isTrue".format(vl))
            print(request.POST['depart'])
            #print(request.POST['depart'].type())
            Create.department = int(departs[request.POST['depart']])

            Create.save()
                    #break
            #    else:print('{}ISwrong'.format(vl))

            return HttpResponseRedirect('/')






class CreateuserView(generic.FormView):
    template_name = "index.html"
    form_class = SelectDepartForm

    def get_context_data(self, **kwargs):
        context = generic.TemplateView().get_context_data(**kwargs)
        depart = Departments.objects.get(id=self.kwargs["pk"])
        #user = CustomUser.objects.get(id=)

    def conxt(self):
        context = {
            'depart_list':Departments.objects.all(),
        }

        return render(self,"App : report",context)

