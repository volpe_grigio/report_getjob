from django.urls import path,include
#from .views import views_simizu,views_saito,views_nihei,views_ise,views_araya
from .views import views_nihei

app_name = 'report'

urlpatterns = [
    #path('',views.IndexView.as_view(),name='index'),
    path('company_list/',views_nihei.CompanyListView.as_view(),name="company_list"),
    path('company_register/',views_nihei.CompanyRegisterView.as_view(),name="company_register"),
    path('tag_register/',views_nihei.TagRegisterView.as_view(),name="tag_register"),
    path('company_detail/<int:pk>',views_nihei.CompanyDetailView.as_view(),name="company_detail"),
    path('offer_register/<int:pk>',views_nihei.OfferRegisterView.as_view(),name="offer_register"),
    path('tag_delete/', views_nihei.TagDeleteView.as_view(),name="tag_delete"),
    path('offer_delete/<int:pk>',views_nihei.OfferDeleteView.as_view(),name="offer_delete"),
]