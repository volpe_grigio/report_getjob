from .models import *
from django import forms

class OfferRegisterForm(forms.ModelForm):
    class Meta:
        model = Offers
        fields = ('company','occupation','offer_pdf')
        widgets = {'company': forms.HiddenInput()
                   }

        def __init__(self, *args, **kwargs):
            super().__init__(*args,**kwargs)
            #self.fields['company'].widget.attrs['readonly'] = True

            for field in self.fields.values():
                field.widget.attrs['class'] = 'form-control'

class TagRegisterForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = ('tag_name',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        for field in self.fields.values():
            field.widget.attrs['class'] = 'form-control'

class CompanyRegisterForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = ('company_name','kana_name','locate','address_number','tag1','tag2','tag3')


        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)

            for field in self.fields.values():
                field.widget.attrs['class'] = 'form-control'