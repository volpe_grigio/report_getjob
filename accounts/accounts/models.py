from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.
#---------------------------------------------------9
#学科
class Departments(models.Model):

    department_name = models.CharField(verbose_name='学科名称',max_length=20,unique=True)

    class Meta:
        verbose_name_plural = 'Departments'

    def __str__(self):
        return self.department_name

#--------------------------------------------------10
#ユーザー
class CustomUser(AbstractUser,models.Model):
    #プルダウンで選ぶようにする

    department = models.ForeignKey(Departments,
                    verbose_name='所属学科',on_delete=models.PROTECT,
                    #db_column="accounts_departments",
                    blank=True,null=True)

    class Meta:
        verbose_name_plural = "CustomUser"





